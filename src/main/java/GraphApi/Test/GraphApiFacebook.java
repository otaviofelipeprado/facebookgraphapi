package GraphApi.Test;

import static com.jayway.restassured.RestAssured.given;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.context.annotation.Configuration;
import org.testng.Assert;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;

@Configuration
public class GraphApiFacebook {

    String accessToken;
    String userEmail = "cgpjypmhfv_1530056623@tfbnw.net";
    String userPass = "admin123";
    String permissions = "user_posts%2Cuser_likes%2Cuser_status%2Cuser_friends%2Cuser_location%2Cuser_photos%2Cpublish_pages%2Cemail%2Cuser_age_range%2Cuser_birthday%2Cuser_friends%2Cuser_gender%2Cuser_photos%2Cuser_posts%2Cuser_tagged_places%2Cuser_videos%2Cgroups_access_member_info%2Cuser_events%2Cuser_managed_groups%2Cpublish_to_groups%2Cads_management%2Cads_read%2Cbusiness_management%2Cread_audience_network_insights%2Cread_insights%2Cpages_manage_cta%2Cpages_manage_instant_articles%2Cpages_show_list%2Cread_page_mailboxes" ;
    String clientId = "2043081469275961";
    String baseUrl = "https://graph.facebook.com/";
    
    @Before
    public void getAcessToken() {
    	System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
    	WebDriver driver=new ChromeDriver();
     
    	driver.get("https://www.facebook.com/v3.0/dialog/oauth?response_type=token&client_id="+ clientId + "&redirect_uri=http://localhost&scope=" + permissions);
    	driver.findElement(By.id("email")).sendKeys(userEmail);
    	driver.findElement(By.id("pass")).sendKeys(userPass);
    	driver.findElement(By.id("loginbutton")).click();
    
    	String urlToken = driver.getCurrentUrl();
    	accessToken = StringUtils.substringBetween(urlToken, "access_token=", "&expires_in");
    	driver.close();
    }
    
    @Test
    public void LoginNoPass(){
    	 System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
    	 WebDriver driver=new ChromeDriver();
    	
    	 driver.get("https://www.facebook.com/v3.0/dialog/oauth?response_type=token&client_id="+ clientId + "&redirect_uri=http://localhost&scope=" + permissions);
    	 driver.findElement(By.id("email")).sendKeys(userEmail);
    	 driver.findElement(By.id("pass")).sendKeys("");
    	 driver.findElement(By.id("loginbutton")).click();
    	    
    	 String urlToken = driver.getCurrentUrl();
    	 accessToken = StringUtils.substringBetween(urlToken, "access_token=", "&expires_in");
    	 driver.close();
    	
    }
    
    public class ResponsePost{
    	String message;
    	String idPost;
    	String messagePost;

		public ResponsePost(String message, String idPost, String messagePost) {
			super();
			this.message = message;
			this.idPost = idPost;
			this.messagePost = messagePost;
		}
    	
    }
    private ResponsePost postTimeLine(){
    	String messagePost = "teste" + accessToken;
    	String message = "{'message': '" + messagePost + "'}";
        String urlPost = "me/feed/?access_token=" + accessToken;
        Response res = given().
                body(message).
                when().
                contentType(ContentType.JSON).
                post(baseUrl + urlPost);
        String idPost = res.
                then().
                contentType(ContentType.JSON).extract().path("id");
        System.out.println(res.getStatusCode());
        System.out.println(res.asString());
        Assert.assertEquals(res.getStatusCode(),200);
        return new ResponsePost(message, idPost, messagePost);
    }
    
    @Test
    public void postUserTimelineSuccess(){
    	ResponsePost responsePost = postTimeLine();
    	
        String urlPost = responsePost.idPost + "/?access_token=" + accessToken;
        System.out.println(urlPost);
        Response res = given().
                when().
                contentType(ContentType.JSON).
                get(baseUrl + urlPost);
        System.out.println(res.asString());
        
        ExtractableResponse<Response> extract = res.then().contentType(ContentType.JSON).extract();
        String createdTime = extract.path("created_time");        
        String msg = extract.path("message");
        String id = extract.path("id");
        
        Assert.assertEquals(responsePost.idPost, id);
        Assert.assertEquals(responsePost.messagePost, msg);
        Assert.assertNotNull(createdTime);
        Assert.assertEquals(res.getStatusCode(),200);
    }
    
    @Test
    public void postUserTimelineNoAccessToken(){
     	String message = "{'message': 'teste" + accessToken + "'}";
        String urlPost = "me/feed/";
        
        Response res = given().
                body(message).
                when().
                contentType(ContentType.JSON).
                post(baseUrl + urlPost);
        System.out.println(res.asString());
        
        ExtractableResponse<Response> extract = res.then().contentType(ContentType.JSON).extract();
		String msg = extract.path("error.message");
		String type = extract.path("error.type");
		int code = extract.path("error.code");
		String traceId = extract.path("error.fbtrace_id");		

		Assert.assertEquals(msg, "An active access token must be used to query information about the current user.");
        Assert.assertEquals(res.getStatusCode(),400);
        Assert.assertEquals(type, "OAuthException");
        Assert.assertEquals(code, 2500);
        Assert.assertNotNull(traceId);        
    }

    @Test
    public void UpdatePostSuccess(){
    	ResponsePost responsePost = postTimeLine();
    	
    	String updatedUrl = responsePost.idPost + "/?access_token=" + accessToken;
        
    	Response res = given().
                body(responsePost.message).
                when().
                contentType(ContentType.JSON).
                post(baseUrl + updatedUrl);
    	System.out.println(res.asString());
        
    	ExtractableResponse<Response> extractPost = res.then().contentType(ContentType.JSON).extract();
        String idPost = extractPost.path("id");
        String urlGet = idPost + "/?access_token=" + accessToken;

        Response res2 = given().
                when().
                contentType(ContentType.JSON).
                get(baseUrl + urlGet);
        System.out.println(res2.asString());
        
        ExtractableResponse<Response> extractGet = res2.then().contentType(ContentType.JSON).extract();
		String msg = extractGet.path("error.message");

	    Assert.assertEquals(responsePost.messagePost, msg);
        Assert.assertEquals(res.getStatusCode(),200);
    }
}